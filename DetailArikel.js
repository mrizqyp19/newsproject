import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import ListNews from "./ListNews";

const image = {};

export default function App() {
  return (
    <View style={styles.container}>
      <ImageBackground style={styles.image}>
        <View style={styles.navBar}>
          <Image source={require("./KinyotNews.png")} style={styles.logo} />
          <View style={styles.rightNav}>
            <TouchableOpacity>
              <Icon name="search" style={styles.navItem} size={25} />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon name="account-circle" style={styles.navItem} size={25} />
            </TouchableOpacity>
          </View>
        </View>
        <Text>DETAIL ARTIKEL</Text>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    backgroundColor: "#3CD580",
  },
  navBar: {
    height: 85,
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 35,
    justifyContent: "space-between",
  },
  logo: {
    width: 88,
    height: 72,
  },
  rightNav: {
    flexDirection: "row",
  },
  navItem: {
    marginLeft: 25,
  },
});
