export const FETCH_REQUEST = "FETCH_REQUEST";
export const FETCH_SUCCES = "FETCH_SUCCES";
export const FETCH_FAILED = "FETCH_FAILED";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
