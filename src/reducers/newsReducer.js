import {
  FETCH_REQUEST,
  FETCH_SUCCES,
  FETCH_FAILED,
} from "../actions/actionType";

const initialState = {
  isLoading: false,
  newsData: [],
  isFailed: {},
};

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REQUEST:
      return {
        ...state,
        isLoading: true,
        isFailed: false,
      };
    case FETCH_SUCCES:
      return {
        ...state,
        isLoading: false,
        isFailed: false,
        newsData: [...state.newsData, ...action.payload],
      };
    case FETCH_FAILED:
      return {
        ...state,
        isLoading: false,
        isFailed: true,
      };
    default:
      return state;
  }
};

export default newsReducer;
