import { LOGIN, LOGOUT } from "../actions/actionType";

const initialState = {
  isLoading: false,
  token: "",
  isFailed: {},
  isLoggin: false,
  nama: "",
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.token,
        isFailed: false,
        isLoggin: true,
        nama: action.payload,
      };
    case LOGOUT:
      return {
        ...state,
        isLoggin: false,
        isFailed: false,
        token: null,
        nama: null,
      };

    default:
      return state;
  }
};

export default authReducer;
