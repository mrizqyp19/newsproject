import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import newsReducer from "./newsReducer";
import authReducer from "./authReducer";

const reducers = combineReducers({
  news: newsReducer,
  auth: authReducer,
});

export default reducers;
