import React, { Component } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Login from "../Login";
import Register from "../Registrasi";
import Home from "../Homes";
import Search from "../Search";
import Profile from "../Profile";
import { connect } from "react-redux";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { AsyncStorage } from "react-native";
import { LOGIN } from "./actions/actionType";
import DetailArikel from "../DetailArikel";

const Tabs = createBottomTabNavigator();

const Logins = createStackNavigator();
const DetailArikels = createStackNavigator();

class Main extends Component {
  componentDidMount = async () => {
    let userToken;
    userToken = null;
    try {
      userToken = await AsyncStorage.getItem("KEY");
      let parsed = JSON.parse(userToken);
      console.log(parsed.token);

      this.props.dispatch({
        type: LOGIN,
        token: parsed.token,
        nama: parsed.userName,
      });
    } catch (e) {}
  };
  render() {
    const { auth } = this.props;
    return (
      <NavigationContainer>
        {auth.token ? (
          <Tabs.Navigator
            tabBarOptions={{
              activeTintColor: "black",
              style: {
                borderTopWidth: 0,
                paddingTop: 3,
                paddingBottom: 4,
                height: 60,
                shadowColor: "#000",
                shadowOpacity: 0.1,
                shadowRadius: 20,
                shadowOffset: { width: 0, height: 0 },
                backgroundColor: "#3CD580",
              },
            }}
          >
            <Tabs.Screen
              name="Home"
              component={Home}
              options={{
                tabBarLabel: "Home",
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons
                    name="home"
                    color={color}
                    size={size}
                  />
                ),
              }}
            />
            <Tabs.Screen
              name="Search"
              component={Search}
              options={{
                tabBarLabel: "Search",
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons
                    name="feature-search-outline"
                    color={color}
                    size={size}
                  />
                ),
              }}
            />
            <Tabs.Screen
              name="Profile"
              component={Profile}
              options={{
                tabBarLabel: "About",
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons
                    name="face-profile"
                    color={color}
                    size={size}
                  />
                ),
              }}
            />
            {/* <DetailArikels.Navigator>
              <DetailArikels.Screen name="Artikel" component={DetailArikel} />
            </DetailArikels.Navigator> */}
          </Tabs.Navigator>
        ) : (
          <Logins.Navigator screenOptions={{ headerShown: false }}>
            <Logins.Screen name="Log In" component={Login} />
            <Logins.Screen name="Sign Up" component={Register} />
          </Logins.Navigator>
        )}
      </NavigationContainer>
    );
  }
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(Main);
