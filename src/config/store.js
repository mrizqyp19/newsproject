import { AsyncStorage } from "react-native";
import { createStore, applyMiddleware } from "redux";
import reducers from "../reducers";
import { reduxForm } from "redux-form";
import thunk from "redux-thunk";

export default configureStore = () => {
  return createStore(reducers, applyMiddleware(thunk));
};
