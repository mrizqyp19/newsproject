import * as React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
// import data from "./data.json";

import ListNews from "./ListNews";

const DEVICE = Dimensions.get("window");

const FirstRoute = () => (
  <SafeAreaView>
    <ScrollView>
      <View style={[styles.scene, { backgroundColor: "#3CD580" }]}>
        <ListNews />
      </View>
    </ScrollView>
  </SafeAreaView>
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#3CD580" }]} />
);

const initialLayout = { width: Dimensions.get("window").width };

function TabViewExample() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "first", title: "TOP NEWS" },
    { key: "second", title: "FOR YOU" },
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });
  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: "white" }}
      style={{ backgroundColor: "#3CD580" }}
    />
  );

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      renderTabBar={renderTabBar}
    ></TabView>
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  itemContainer: {
    margin: 15,
    borderRadius: 16,
    height: 160,
    width: DEVICE.width * 0.88,
    // alignItems: "center",
    backgroundColor: "#29BB6A",
    flexDirection: "row",
  },
  title: {
    color: "white",
    margin: 8,
    fontSize: 15,
    fontStyle: "italic",
  },
  sourceName: {
    color: "white",
  },
});

export default TabViewExample;
