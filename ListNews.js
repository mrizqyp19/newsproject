import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions, Image } from "react-native";

const DEVICE = Dimensions.get("window");
import Axios from "axios";
import {
  FETCH_REQUEST,
  FETCH_SUCCES,
  FETCH_FAILED,
} from "./src/actions/actionType";
import { connect } from "react-redux";
import { compose } from "redux";
import { TouchableOpacity } from "react-native-gesture-handler";
class ListItem extends React.Component {
  constructor() {
    super();
    this.state = {
      artikel: [],
    };
  }
  componentDidMount() {
    this.fetchApi();
  }

  async fetchApi() {
    try {
      this.props.dispatch({
        type: FETCH_REQUEST,
      });
      Axios.get(
        "http://newsapi.org/v2/top-headlines?country=id&apiKey=c76ed42edbb04fc8a7702a1084d582d9"
      ).then((response) => {
        this.props.dispatch({
          type: FETCH_SUCCES,
          payload: response.data.articles,
        });
      });
    } catch (err) {
      this.props.dispatch({
        type: FETCH_FAILED,
        payload: err,
      });
      console.log(err);
    }
  }

  WholeNews() {
    const { news } = this.props;

    return news.newsData.map(function (news, i) {
      return (
        <TouchableOpacity style={styles.itemContainer} key={i}>
          <View style={{ width: DEVICE.width * 0.55 }}>
            <Text style={styles.title}>{news.title}</Text>

            <View
              style={{
                flexDirection: "row",
                margin: 5,
                paddingTop: 20,
                paddingHorizontal: 20,
              }}
            >
              <Text style={styles.sourceName}>{news.source.name}</Text>
              <Text style={styles.sourceName}> * 11</Text>
            </View>
          </View>
          <View style={{ alignSelf: "center", margin: 10 }}>
            <Image
              source={{ uri: news.urlToImage }}
              style={{ width: 100, height: 100, borderRadius: 10 }}
            />
          </View>
        </TouchableOpacity>
      );
    });
  }

  render() {
    return <View>{this.WholeNews()}</View>;
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  itemContainer: {
    margin: 15,
    borderRadius: 16,
    height: 160,
    width: DEVICE.width * 0.88,

    backgroundColor: "#29BB6A",
    flexDirection: "row",
  },
  title: {
    color: "white",
    margin: 8,
    fontSize: 15,
    fontStyle: "italic",
  },
  sourceName: {
    color: "white",
  },
});
const mapStateToProps = (state) => ({
  news: state.news,
});

export default connect(mapStateToProps)(ListItem);
