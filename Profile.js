import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
const image = {};
import { connect } from "react-redux";
import { LOGOUT } from "./src/actions/actionType";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
    };
  }
  Logout = async () => {
    // console.log(this.state.userName, " ", this.state.password);
    try {
      await AsyncStorage.removeItem("KEY");
      this.props.dispatch({
        type: LOGOUT,
      });
    } catch (e) {}
  };

  componentDidMount = async () => {
    try {
      userToken = await AsyncStorage.getItem("KEY");
      const parsed = JSON.parse(userToken);
      this.setState({ userName: parsed.userName });
    } catch (e) {}
  };

  render() {
    console.log(this.props);
    const { auth } = this.props;
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.image}>
          <View style={styles.navBar}>
            <Image source={require("./KinyotNews.png")} style={styles.logo} />
          </View>
          <View style={styles.navBarr}>
            <View
              style={{
                height: 100,
                width: 180,
              }}
            >
              <Text style={{ fontSize: 30, color: "black", fontWeight: "600" }}>
                {this.state.userName}
              </Text>
            </View>
            <Image
              source={require("./dummy.jpg")}
              style={{ width: 110, height: 110, borderRadius: 50 }}
            />
            <View
              style={{
                position: "absolute",
                height: 6,
                width: 150,
                backgroundColor: "white",
                alignSelf: "flex-end",
                borderRadius: 8,
              }}
            />
          </View>
          <View style={styles.containerText}>
            <MaterialIcons name="favorite" size={30} color="black" />
            <Text style={{ fontSize: 20 }}>Favorites</Text>
            <MaterialIcons name="navigate-next" size={30} color="black" />
          </View>
          <View style={styles.containerText}>
            <MaterialIcons name="notifications" size={30} color="black" />
            <Text style={{ fontSize: 20 }}>notifications</Text>
            <MaterialIcons name="navigate-next" size={30} color="black" />
          </View>
          <TouchableOpacity onPress={() => this.Logout()}>
            <View style={styles.containerText}>
              <MaterialCommunityIcons name="logout" size={30} color="red" />
              <Text style={{ fontSize: 20, color: "red" }}>Log out</Text>
              <MaterialIcons name="navigate-next" size={30} color="red" />
            </View>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    backgroundColor: "#3CD580",
  },
  navBarr: {
    height: 150,
    backgroundColor: "#55E394",
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  navBar: {
    height: 85,
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 35,
    justifyContent: "center",
  },
  logo: {
    width: 88,
    height: 82,
  },
  containerText: {
    height: 70,
    backgroundColor: "#55E394",
    elevation: 3,
    marginTop: 10,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
});

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(Profile);
