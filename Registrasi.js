import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/MaterialIcons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { LOGIN } from "./src/actions/actionType";

const image = {};

class Registrasi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
  }

  signIn() {
    this.props.navigation.navigate("Log In");
  }
  signUp = async () => {
    const user = this.state.username;
    const pass = this.state.password;
    console.log(user, " ", pass);
    if (!user && !pass) {
      this.setState({ isError: true });
    } else if (user && pass) {
      const userToken = "Aww";
      try {
        this.props.dispatch({
          type: LOGIN,
          payload: this.state.username,
          token: userToken,
        });
        var simpen = {
          token: userToken,
          userName: user,
        };
        await AsyncStorage.setItem("KEY", JSON.stringify(simpen));
      } catch (e) {}
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={require("./bg.png")} style={styles.image}>
          <Image
            source={require("./KinyotNews.png")}
            style={{ width: 220, height: 220 }}
          />
          <View style={styles.formLogin}>
            <View style={{ margin: 30 }}>
              <View style={styles.containerLogin}>
                <Text style={{ fontSize: 20 }}>Username</Text>
                <View style={styles.form}>
                  <View style={{ flexDirection: "column" }}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Masukkan Username"
                      onChangeText={(username) => this.setState({ username })}
                    />
                    <View
                      style={{
                        height: 2,
                        width: 200,
                        backgroundColor: "black",
                      }}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.containerLogin}>
                <Text style={{ fontSize: 20 }}>Email</Text>
                <View style={styles.form}>
                  <View style={{ flexDirection: "column" }}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Masukkan Email"
                      // onChangeText={(userName) => this.setState({ userName })}
                    />
                    <View
                      style={{
                        height: 2,
                        width: 200,
                        backgroundColor: "black",
                      }}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.containerLogin}>
                <Text style={{ fontSize: 20 }}>Password</Text>
                <View style={styles.form}>
                  <View style={{ flexDirection: "column" }}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Masukkan Password"
                      onChangeText={(password) => this.setState({ password })}
                      secureTextEntry={true}
                    />
                    <View
                      style={{
                        height: 2,
                        width: 200,
                        backgroundColor: "black",
                      }}
                    />
                  </View>
                </View>
              </View>
              <View>
                <Text style={{ fontSize: 20 }}>Confirm Password</Text>
                <View style={styles.form}>
                  <View style={{ flexDirection: "column" }}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Masukkan Password"
                      // onChangeText={(password) => this.setState({ password })}
                      secureTextEntry={true}
                    />
                    <View
                      style={{
                        height: 2,
                        width: 200,
                        backgroundColor: "black",
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              justifyContent: "center",
              flexDirection: "row",
              margin: 30,
            }}
          >
            <View>
              <TouchableOpacity onPress={() => this.signUp()}>
                <View style={styles.buttonSignin}>
                  <Text
                    style={{ fontSize: 16, fontWeight: "600", color: "white" }}
                  >
                    SIGN UP
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.signIn()}>
                <Text>Already Have an Account?</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  formLogin: {
    height: 400,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: "#BCBABA",
    width: 316,
    backgroundColor: "#FFFFFF",
    overflow: "hidden",
    borderRadius: 16,
    marginLeft: 22,
  },
  form: {
    flexDirection: "row",
  },
  containerLogin: {
    paddingBottom: 30,
  },
  forgot: {
    color: "blue",
    justifyContent: "center",
  },
  buttonSignin: {
    width: 149,
    height: 48,
    backgroundColor: "#6497D3",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 16,
  },
});

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps)(Registrasi);
