import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
import { connect } from "react-redux";
import Home from "./Home";
const image = {};

class Homes extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.image}>
          <View style={styles.navBar}>
            <Image source={require("./KinyotNews.png")} style={styles.logo} />
          </View>
          <Home />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    backgroundColor: "#3CD580",
  },
  navBar: {
    height: 85,
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 35,
    justifyContent: "center",
  },
  logo: {
    width: 88,
    height: 82,
  },
  rightNav: {
    flexDirection: "row",
  },
  navItem: {
    marginLeft: 25,
  },
});

export default Homes;
