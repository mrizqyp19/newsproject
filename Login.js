import { StatusBar } from "expo-status-bar";
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import { connect } from "react-redux";
import { LOGIN } from "./src/actions/actionType";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      isError: false,
    };
  }

  signUp() {
    this.props.navigation.navigate("Sign Up");
  }

  signIn = async () => {
    const user = this.state.userName;
    const pass = this.state.password;
    console.log(user, " ", pass);
    if (!user && !pass) {
      this.setState({ isError: true });
    } else if (user && pass) {
      const userToken = "Aww";
      try {
        this.props.dispatch({
          type: LOGIN,
          payload: this.state.userName,
          token: userToken,
        });
        var simpen = {
          token: userToken,
          userName: user,
        };
        await AsyncStorage.setItem("KEY", JSON.stringify(simpen));
      } catch (e) {}
    }
  };

  render() {
    const { auth } = this.props;
    console.log(auth);

    return (
      <View style={styles.container}>
        <ImageBackground source={require("./bg.png")} style={styles.image}>
          <Image
            source={require("./KinyotNews.png")}
            style={{ width: 220, height: 220 }}
          />
          <View style={styles.formLogin}>
            <View style={{ margin: 30 }}>
              <View style={styles.containerLogin}>
                <Text style={{ fontSize: 20 }}>Username/Email</Text>
                <View style={styles.form}>
                  <Icon name="account-circle" color="black" size={30} />
                  <View style={{ flexDirection: "column" }}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Masukkan Nama User/Email"
                      keyboardType="email-address"
                      onChangeText={(userName) => this.setState({ userName })}
                    />
                    <View
                      style={{
                        height: 2,
                        width: 200,
                        backgroundColor: "black",
                      }}
                    />
                  </View>
                </View>
              </View>
              <View>
                <Text style={{ fontSize: 20 }}>Password</Text>
                <View style={styles.form}>
                  <Icon name="lock" color="black" size={30} />
                  <View style={{ flexDirection: "column" }}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Masukkan Password"
                      onChangeText={(password) => this.setState({ password })}
                      secureTextEntry={true}
                    />
                    <View
                      style={{
                        height: 2,
                        width: 200,
                        backgroundColor: "black",
                      }}
                    />
                  </View>
                </View>
              </View>
              <Text
                style={
                  this.state.isError ? styles.errorText : styles.hiddenErrorText
                }
              >
                Username dan Password harus Diisi
              </Text>
            </View>
          </View>

          <View
            style={{
              justifyContent: "flex-end",
              flexDirection: "row",
              paddingHorizontal: 20,
            }}
          >
            <Text style={styles.forgot}>Forgot Password?</Text>
          </View>
          <View
            style={{
              padding: 25,
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.signIn();
              }}
            >
              <View style={styles.buttonSignin}>
                <Text
                  style={{ fontSize: 16, fontWeight: "600", color: "white" }}
                >
                  SIGN IN
                </Text>
              </View>
            </TouchableOpacity>
            <MaterialCommunityIcons name="google" size={46} color="black" />
            <MaterialCommunityIcons name="twitter" size={46} color="black" />
          </View>
          <View
            style={{
              justifyContent: "center",
              flexDirection: "row",
              paddingTop: 20,
            }}
          >
            <Text>Dont Have an Account? </Text>
            <TouchableOpacity onPress={() => this.signUp()}>
              <Text style={styles.forgot}>SIGN UP</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  formLogin: {
    height: 250,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: "#BCBABA",
    width: 316,
    backgroundColor: "#FFFFFF",
    overflow: "hidden",
    borderRadius: 16,
    margin: 30,
  },
  form: {
    flexDirection: "row",
  },
  containerLogin: {
    paddingBottom: 30,
  },
  forgot: {
    color: "blue",
    justifyContent: "center",
  },
  buttonSignin: {
    width: 149,
    height: 48,
    backgroundColor: "#6497D3",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 16,
  },
  errorText: {
    color: "red",
    textAlign: "center",
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: "transparent",
    textAlign: "center",
    marginBottom: 16,
  },
});

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps)(Login);
